# 1 TDD
    - Write test first
    - Test fail
    - Write logic later to pass the test
# 2 Get Started

    * Some unit testing framework:
        - JUnit for Java
        - NUnit for .Net
        - Mocha for NodeJS
        - PyUnit for python
        - CppUnit for C++
    * Assertion:
        - Stating something to be true.
    * Using assertion in programming
        - Use Assert keyword
# 3 Working with test

    * Quick summary testing life cycle:
        - Red: make the test fail
        - Green: make the test pass
        - Gray: refactor            
    * TDD is a nutsheel:
        - Write a test
        - Watch the test fail
        - Write application logic
        - Pass the test
        - Refactor, remove duplication
        - Pass the test again

# 4 Individual techniques
    * 3 rules:
        - Arrange
        - Act
        - Assert
    * Expected exception
        - Testing a method that can be throw some exception
    * Setting up and tearing down
        - Set up : run before test case run
        - Tear down: run after test case run
    * The order of tests can't be controlled
# 5 Additional topic
    * Using mock:
        - Minimize the dependencies between currently testing unit and anything else
        - Assume every part of testing unit is perfect & reliable, focus only on test
    * Reason for using Mock object:
        - Real object hasn't been written yet
        - Calling a UI/ needs human interaction
        - Slow / Difficult to set up
        - External resource
        - Non-deterministic behavior
    * Fake Object vs Mock object

        - Fake Object:
            + Match the original method implementation
            + Return pre-arranged result
        - Mock Object: also verify interaction
            + Assert the expected values from unit under test
    * Recommendation:
        - One test case / test fixture for each class
        - 3-5 test methods for each class method
        - Test for every branch
        - Use coverage tools
    * Avoid:
        - Interact with db/ file system
        - Require non-trivial network communication
        - Require environment changes to run
        - Call complex collaborator object
    * 2 way approach
        - Create complete suite of unit test (usually bad idea)
        - Create test as needed
            + First, validate existing functionality
            + Red, Green, Refactor